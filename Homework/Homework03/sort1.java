import java.util.Arrays;
import java.util.Collections;

public class sort1 {
    public static void main(String[] args) {
        Integer[] hw = { 34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20 };

            System.out.print("Исходник:      ");
//            System.out.print(Arrays.toString(hw));
            for (int value : hw)    {
                System.out.print(value + ", ");
            }

            System.out.println();

        //Сортировка массива
        System.out.print("Отсортировано: ");
        Arrays.sort(hw, Collections.reverseOrder());
        for (int values : hw) {
            System.out.print(values + ", ");

        }
    }
}
