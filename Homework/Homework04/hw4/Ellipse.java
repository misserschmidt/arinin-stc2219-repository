package homeworks.hw4;

public class Ellipse extends Figure {
    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimeter(int a, int b) {
        double perimeter = 4 * (((Math.PI * a * b) + Math.pow(a - b, 2)) / (a + b));
        double result = Math.ceil(perimeter); // Простое округление
        return result;
    }
}
