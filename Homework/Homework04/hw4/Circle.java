package homeworks.hw4;

public class Circle extends Ellipse implements Moveable {
    public Circle(int x, int y) {
        super(x, y);
    }
    @Override
    public void moove(int x, int y) {
        x = x + randomNumber;
        y = y + randomNumber / 2;
        setX(x);
        setY(y);
    }

    //implement Figure
    @Override
    public double getPerimeter(int a, int b) {
        double perimeter = 2 * Math.PI * a;
        double scale = Math.pow(10, 3);
        double result = Math.ceil(perimeter * scale) / scale; // более точное округление
            return result;
    }

    //random
    int randomMax = 100;
    int randomNumber = random(randomMax);
    public int random(int randomMax) {
        return (int) (Math.random() * randomMax);
    }



}



