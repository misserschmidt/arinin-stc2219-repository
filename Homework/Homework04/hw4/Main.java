package homeworks.hw4;

import java.util.Scanner;

    public class Main {
        public static void main(String[] args) {
            Rectang rectangle = new Rectang(0, 0);
            Ellipse ellipse = new Ellipse(0, 0);
            Square square = new Square(0,0);
            Circle circle = new Circle(0,0);

            Scanner scanner = new Scanner(System.in);
            int rectangleA = 0;
            int rectangleB = 0;
            int ellipseA = 0;
            int ellipseB = 0;
            int squareA = 0;
            int circleA = 0;

            //Создание массива всех фигур
            Figure[] figures = {rectangle, ellipse, square, circle};

            //Ввод характеристик фигур
            for (int j = 0; j < figures.length; j++) {
                if(figures[j] == rectangle) {
                    System.out.println("Введите значение стороны А прямоугольника: ");
                    rectangleA = scanner.nextInt();
                    System.out.println("Введите значение стороны B прямоугольника: ");
                    rectangleB = scanner.nextInt();
                }
                else if(figures[j] == ellipse) {
                    System.out.println("Введите значение длины полуоси А эллипса: ");
                    ellipseA = scanner.nextInt();
                    System.out.println("Введите значение длины полуоси B эллипса: ");
                    ellipseB = scanner.nextInt();
                }
                else if(figures[j] == square) {
                    System.out.println("Введите значение стороны квадрата: ");
                    squareA = scanner.nextInt();
                }
                else if(figures[j] == circle) {
                    System.out.println("Введите значение радиуса круга: ");
                    circleA = scanner.nextInt();
                }
                else {
                    return;
                }
            }

            //Вывод значений
            for (int i = 0; i < figures.length; i++) {
                if(figures[i] == rectangle) {
                    System.out.println("Периметр прямоугольника = " + rectangle.getPerimeter(rectangleA, rectangleB));
                }
                else if(figures[i] == ellipse) {
                    System.out.println("Периметр эллипса = " + ellipse.getPerimeter(ellipseA, ellipseB));
                }
                else if(figures[i] == square) {
                    System.out.println("Периметр квадрата = " + square.getPerimeter(squareA, squareA));
                    square.moove(square.getX(), square.getY());
                    System.out.println("Новые случайные координаты квадрата:"  + "\n" + "x = " + square.getX() + "\n" + "y = " + square.getY());
                }
                else if(figures[i] == circle) {
                    System.out.println("Длина окружности = "  + circle.getPerimeter(circleA, circleA));
                    circle.moove(circle.getX(), circle.getY());
                    System.out.println("Новые случайные координаты круга:"  + "\n" + "x = "  + circle.getX() + "\n" + "y = " + circle.getY());
                }
                else {
                    return;
                }
            }
        }
    }


