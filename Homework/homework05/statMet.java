package homeworks.hw5;

import java.util.Scanner;

public class statMet {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива чисел: ");
        int arrayLength = scanner.nextInt();
        Integer[] array = new Integer[arrayLength];
        fillArray(array);
        System.out.println("Произведено сложение всех чисел массива: ");
        int additionResult = additionOfNumbers(array);
        System.out.println(additionResult);
        int maxValue = getMaxValue(array);
        System.out.println("наибольшее число массива:\n" + maxValue);
        System.out.println("Произведено вычитание всех чисел массива из наибольшего: ");
        int subtractionResult = subtractionOfNumbers(array);
        System.out.println(subtractionResult);
        int multiplicationResult = multiplicationOfNumbers(array);
        System.out.println("Произведено умножение всех чисел массива:\n" + multiplicationResult);
        int divisionResult = divisionOfNumbers(array);
        System.out.println("Произведено деление всех чисел массива:\n" + divisionResult);
        System.out.println("Введите число для вычисления факториала:");
        Integer factorialResult = getFactorial(scanner.nextInt());
        System.out.println("Факториал числа равен:\n"  + + factorialResult);
    }

    public static void fillArray(Integer[] array) {
        System.out.println("Введите числа для заполнения массива: ");
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            System.out.print(i + " Элемент = ");
            int input = scanner.nextInt();
            array[i] = input;
        }
    }

    public static int additionOfNumbers(Integer[] array) {
        int sum = 0;
        for (int j : array) {
            sum += j;
        }
        return sum;
    }

    public static int getMaxValue(Integer[] numbers){
        int maxValue = numbers[0];
        for (int i = 1;  i < numbers.length; i++){
            if ( numbers[i] > maxValue){
                maxValue = numbers[i];
            }

        }
        return maxValue;
    }


    public static int subtractionOfNumbers(Integer[] numbers) {

        int sum = 0;
        int maxValue = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] > maxValue) {
                maxValue = numbers[i];
            }
            sum += numbers[i];
        }

       return maxValue - (sum - maxValue);
    }



        public static int multiplicationOfNumbers(Integer[] array) {
        int product = 1;
        for (int j : array) {
            product *= j;
        }
        return product;
    }

    public static int divisionOfNumbers(Integer[] array) {  // дальше пока не придумал
        int result = 1;
        for (int j : array) {
            result /= j;
        }
        return result;

        }



        public static int getFactorial(int f) {

        int result = 1;
        for (int i = 1; i <= f; i++){
            result = result*i;
        }
        return result;
    }
}