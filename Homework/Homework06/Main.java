package homeworks.hw6;

import java.util.Arrays;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число:");
        int value = scanner.nextInt();
        int [] valueInArray = new int[1];
        valueInArray[0] = value;


        String valueToString = Integer.toString(value);
        int[] array = new int[valueToString.length()];
        for (int i = valueToString.length() - 1; i >= 0; i--) {
            array[i] = value % 10;
            value /= 10;
        }
        System.out.println("Результат преобразования в массив:");
        System.out.println(Arrays.toString(array));

        //проверка на четность результата сложения
        Sequence.filter(array, (numbers -> {
            int sumArrayElements = 0;
            for (int i = 0; i < array.length; i++) {
                sumArrayElements = sumArrayElements + array[i];
                if (i == array.length - 1) {
                    System.out.println();
                }
            }
            System.out.print("Сумма всех цифр массива " + Arrays.toString(array) + " = ");
            System.out.println(sumArrayElements);
            if (sumArrayElements % 2 == 0) {
                System.out.println(  "Сумма всех цифр " + valueInArray[0] + " - чётное число " + sumArrayElements );
            }
            else {
                System.out.println("\n" + "Сумма всех цифр числа " + valueInArray[0] + " - нечётное число " + sumArrayElements);
            }
            return true;
        }));

        //проверка на чётность
        Sequence.filter(valueInArray, (numbers -> {
            if (valueInArray[0] % 2 == 0) {
                System.out.print("Число " + valueInArray[0] + " - чётное");
            }
            else {
                System.out.print( "Число " + valueInArray[0] + " - нечётное");
            }
            return true;
        }));


        //проверка на палиндром
        Sequence.filter(array, (numbers -> {
            boolean t = true;
            for (int i = 0; i < (array.length + 1) / 2 && t; i++)
                t = array[i] == array[array.length - i - 1];
            System.out.print(t ? ", палиндром." : ", не палиндром.");
            return true;
        }));

    }
}
