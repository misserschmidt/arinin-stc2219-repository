import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ScannerPatch {

    private Path path;

    public Path getPath() {
        return path;
    }

    public void askPath() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter file ...");

        while (scanner.hasNext()) {
            String fileName = scanner.nextLine();
            File tempFile = new File(fileName);

            if (tempFile.isFile()) {
                path = Paths.get(fileName);
                return;
            }
            System.out.println("File not found");
    }
}
}
