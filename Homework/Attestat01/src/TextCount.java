import java.nio.file.Path;


public class TextCount {

    private final Path path;
    private final String wordsFileName = "WordsCo.txt";
    private final String symbFileName = "SymbsCo.txt";


    ScannerPatch scannerPatch = new ScannerPatch();

    {
        scannerPatch.askPath();
        path = scannerPatch.getPath();
    }

    ReadFile readFile = new ReadFile(path);
    TextParser textParser = new  TextParser(readFile.readLines());

    public void wordsParsWrite() {
        System.out.println("File summ word...");
        WriteFile writeFile = new WriteFile(path, wordsFileName, textParser.wordParser());
        writeFile.writeMap();
    }

    public void symbParseWrite() {
        System.out.println("File summ symb...");
        WriteFile writeFile = new WriteFile(path, symbFileName, textParser.symbParsing());
        writeFile.writeMap();
    }

}
