import java.util.HashMap;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class TextParser {
    private final List<String> lines;

    public TextParser(List<String> lines) {
        this.lines = lines;
    }

    public Map<String, Integer> wordParser() {
        Map<String, Integer> resultMap = new HashMap<>();
        lines.stream()
                .map(line -> line.replaceAll("[^а-яёА-ЯЁ -]+", ""))
                .map(String::toLowerCase)
                .map(line -> line.split(" "))
                .forEach(words -> Arrays.stream(words)
                .forEach(word -> {
                    if (!(word.equals("") | word.equals("-"))) {
                        if (!resultMap.containsKey(word)) {
                            resultMap.put(word, 1);
                        }
                        else {
                            resultMap.put(word, resultMap.get(word) + 1);
                        }
                    }
                }));
        return resultMap;

    }

    public Map<String, Integer> symbParsing() {
        Map<String, Integer> resultMap = new HashMap<>();

        lines.stream()
                .map(line -> line.replaceAll("[^а-яёА-ЯЁ]+", ""))
                .map(String::toLowerCase)
                .map(line -> line.split(""))
                .forEach(letters -> Arrays.stream(letters)
                        .forEach(letter -> {
                    if (!letter.equals("")) {
                        if (!resultMap.containsKey(letter)) {
                            resultMap.put(letter, 1);
                        } else {
                            resultMap.put(letter, resultMap.get(letter) + 1);
                        }
                    }
                }));
    return resultMap;
    }
}
