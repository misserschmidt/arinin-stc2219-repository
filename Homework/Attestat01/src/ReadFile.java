import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

public class ReadFile {
    private final Path path;

    public ReadFile(Path path) {
        this.path = path;
    }

    public List<String> readLines() {

        List<String> lines;

        try {
            lines = Files.readAllLines(path);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return lines;
    }
}
