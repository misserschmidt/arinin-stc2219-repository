
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.Collator;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Collectors;


public class WriteFile {
    private Path path;
    private final String newFileName;
    private final Map<String, Integer> resultMap;

    public WriteFile(Path path, String newFileName, Map<String, Integer> resultMap) {
        this.path = path;
        this.newFileName = newFileName;
        this.resultMap = resultMap;
    }

    public void writeMap() {
        if (path.getParent().equals(Paths.get("C:\\"))) {
            path = path.getFileName();
        }

        Path newFilePath = path.getParent() == null ?
                Paths.get(newFileName) :
                Paths.get(path.getParent() + "/" + newFileName);

        Collator collator = Collator.getInstance(new java.util.Locale("ru", "RU"));

        try {
            Path newFile = Files.notExists(newFilePath) ? Files.createFile(newFilePath) : newFilePath;
            Files.write(newFile, resultMap.entrySet()
                    .stream()
                    .map(k -> k.getKey() + " - " + k.getValue())
                    .sorted(Comparator.comparing(s -> s, collator))
                    .collect(Collectors.toList()));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println(newFilePath.toAbsolutePath());

    }
}

